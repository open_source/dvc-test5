import pickle
import pandas as pd
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from catboost import Pool, CatBoostClassifier
from sklearn.metrics import classification_report
from pathlib import Path
import sys

import yaml
import os
import json
import re

def has_cyrillic(text):
    return bool(re.search('[а-яА-Я]', text))

if __name__ == '__main__':
    path_params_parts =  os.path.abspath(__file__).split('\\')
    path_params_parts.remove(path_params_parts[len(path_params_parts)-1])
    path_dir = '\\'.join(path_params_parts)
    path_params = path_dir + "\\params.yaml"

    with open(path_params, 'r', encoding='utf8') as fd:
        params = yaml.safe_load(fd)

    path_text = Path(params['path_text'])
    path_bert = Path(params['path_bert'])
    path_w2v = Path(params['path_w2v'])
    path_lda = Path(params['path_lda'])
    n_estimators = int(params['n_estimators'])
    depth = int(params['depth'])
    test_size = float(params['test_size'])

    with open(str(path_text), 'rb') as f:
        text_table = pickle.load(f)

    with open(str(path_bert), 'rb') as f:
        bert_table = pickle.load(f)

    with open(str(path_w2v), 'rb') as f:
        w2v_table = pickle.load(f)

    with open(str(path_lda), 'rb') as f:
        lda_table = pickle.load(f)

    full_table = text_table.merge(bert_table, on='name').merge(w2v_table, on='name').merge(lda_table, on='name')

    train_data, test_data = train_test_split(full_table, test_size=test_size)

    tr_fea = [fea for fea in train_data if fea not in ['name', 'delo']]

    train_pool = Pool(train_data[tr_fea], label=train_data.delo, text_features=['text'])
    test_pool = Pool(test_data[tr_fea], label=test_data.delo, text_features=['text'])

    cls = CatBoostClassifier(n_estimators=n_estimators, depth=depth, random_seed=100)
    cls.fit(train_pool)

    train_res = classification_report(train_data.delo, cls.predict(train_pool), output_dict=True)
    test_res = classification_report(test_data.delo, cls.predict(test_pool), output_dict=True)

    scores = {}
    for key, value in train_res.items():
        if type(value) is dict:
            for key2, value2 in value.items():
                str1 = key.replace(')', '').replace('(', '')
                if has_cyrillic(str1):
                    continue
                str = f"TRAIN {str1} {key2}"
                scores[str] = value2;
        else:
            scores[f'TRAIN {key} {key2}'] = value2;

    for key, value in test_res.items():
        if type(value) is dict:
            for key2, value2 in value.items():
                str1 = key.replace(')', '').replace('(', '')
                if has_cyrillic(str1):
                    continue
                str = f"TEST {str1} {key2}"
                scores[str] = value2;

        else:
            scores[f'TRAIN {key} {key2}'] = value2;

    cls.save_model(path_dir + '\\catboost_model')

    scores_str = json.dumps(scores, ensure_ascii=False)
    path = Path(path_dir+'\\scores.json')
    path.write_text(scores_str, encoding='utf-8')
    print('data processed')

