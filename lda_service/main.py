import json
from pathlib import Path
import sys

import gensim
import numpy as np
import pandas as pd
from gensim.corpora import Dictionary
from tqdm import tqdm as tqdmn
import time
import pickle
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
import spacy
from gensim.models import KeyedVectors
from queue import Queue
from threading import Thread
import time

import yaml
import os



class calcText:
    def __init__(self, text, name):
        self.name = name
        self.text = text
        self.lemmas=[]
    def pipe(self, nlp, min):
        tokens = list(nlp.pipe([self.text]))[0]
        for token in tokens:
            if token.is_alpha and len(token.lemma_) >= min:
                self.lemmas.append(token.lemma_)


class procWorker(Thread):

    def __init__(self, queue, nlp, min):
        Thread.__init__(self)
        self.queue = queue
        self.nlp = nlp
        self.min = min

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            file = self.queue.get()
            try:
                file.pipe(self.nlp, self.min)
            finally:
                self.queue.task_done()


if __name__ == '__main__':
    path_params_parts =  os.path.abspath(__file__).split('\\')
    path_params_parts.remove(path_params_parts[len(path_params_parts)-1])
    path_dir = '\\'.join(path_params_parts)
    path_params = path_dir + "\\params.yaml"

    with open(path_params, 'r', encoding='utf8') as fd:
        params = yaml.safe_load(fd)


    num_topics = int(params['num_topics'])
    path_data = Path(str(params['pin']))
    path_to_stop_words = Path(str(params['pstop']))
    if params['p_w2v'] is not None:
        path_w2v = Path(str(params['p_w2v']))
    if params['path_lemmas'] is not None:
        path_lemmas = Path(str(params['path_lemmas']))
    min_freq_words = int(params['min'])
    max_freq_words = int(params['max'])
    min_lemma_len = int(params['min_lem'])

    path_lemmas = path_lemmas.absolute()

    if len(str(path_lemmas)) == 0:
        with open(path_data, 'rb') as f:
            data = pickle.load(f)

        print('tokenization, lemmatization')
        queue = Queue()
        # Create 8 worker threads
        for x in tqdmn(range(8)):
            nlp = spacy.load("ru_core_news_sm")
            worker = procWorker(queue, nlp, min_lemma_len)
            # Setting daemon to True will let the main thread exit even though the workers are blocking
            worker.daemon = True
            worker.start()

        calcTexts = []
        for index, value in data.iterrows():
            calc = calcText(value.text, value['name'])
            calcTexts.append(calc)
            queue.put(calc)

        with tqdmn(total=queue.unfinished_tasks) as pbar:
            last_readed_files = queue.unfinished_tasks
            while queue.unfinished_tasks != 0:
                pbar.update(last_readed_files - queue.unfinished_tasks)
                last_readed_files = queue.unfinished_tasks
                time.sleep(0.2)

        queue.join()

        all_lemmas = []
        for lemma_file_list in calcTexts:
            for lemma in lemma_file_list.lemmas:
                all_lemmas.append(lemma)

        word_freq = pd.Series(all_lemmas).value_counts()
        word_freq = list(
            word[0] for word in word_freq.to_dict().items() if min_freq_words < word[1] < max_freq_words)

        word_freq_str = json.dumps(word_freq, ensure_ascii=False)
        path = Path('freq_words.json')
        path.write_text(word_freq_str, encoding='utf-8')

        stop_words = []
        if len(str(path_to_stop_words)) != 0:
            stop_words = json.loads(path_to_stop_words.read_text(encoding='utf-8'))

        print('clearing lemma list')
        lemma_dict_clear = {}
        for lemma_file_list in tqdmn(calcTexts):
            lemma_new_list = []
            for lemma in lemma_file_list.lemmas:
                if not (lemma in stop_words) and (lemma in word_freq) and not (lemma in lemma_new_list):
                    lemma_new_list.append(lemma)
            lemma_file_list.lemmas = lemma_new_list
            lemma_dict_clear[lemma_file_list.name] = lemma_new_list

        df_lemms = pd.DataFrame(lemma_dict_clear.items(), columns=['name', 'lemma'])
        feat_table_full = pd.merge(data, df_lemms, on='name')
        with open(path_dir + '\\lemma_table.pickle', 'wb') as f:
            pickle.dump(feat_table_full[['name', 'lemma']], f)

        lemma_list = list(item for item in feat_table_full.lemma.to_list())
        lemma_list_str = json.dumps(lemma_list, ensure_ascii=False)
        path = Path(path_dir + '\\lemma_list.json')
        path.write_text(lemma_list_str, encoding='utf-8')

    else:
        with open(path_lemmas, 'rb') as f:
            feat_table_full = pickle.load(f)
        lemma_list = list(item for item in feat_table_full.lemma.to_list())

    gensim_dict = Dictionary(lemma_list)

    corpus = [gensim_dict.doc2bow(text) for text in lemma_list]


    print('loading LDA model')
    ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=num_topics, id2word=gensim_dict, passes=20, random_state=100)
    ldamodel.save(path_dir+'\\lda_model')

    file_topics = {}
    for index, value in feat_table_full.iterrows():
        file_topics[value['name']] = dict(ldamodel.get_document_topics(gensim_dict.doc2bow(value['lemma'])))

    pd_file_topic = pd.DataFrame.from_dict(file_topics, orient='index')
    pd_file_topic = pd_file_topic.reindex(sorted(pd_file_topic.columns), axis=1)
    pd_file_topic = pd_file_topic.reset_index().rename({'index': 'name'}, axis=1)
    pd_file_topic.columns = [str(fea) + '_top' if fea not in ['name'] else fea for fea in
                           pd_file_topic.columns]
    pd_file_topic = pd_file_topic.fillna(0)

    new_table = feat_table_full.merge(pd_file_topic, on='name')[pd_file_topic.columns]

    print('data processed')
    with open(path_dir + '\\lda_table.pickle', 'wb') as f:
        pickle.dump(new_table, f)

