from pathlib import Path
import docx
import sys
import pandas as pd
from tqdm import tqdm as tqdmn
from queue import Queue
from threading import Thread
import time
import pickle
import yaml
import os

class FileData:

    def __init__(self, name, filepath):
        self.par_list = None
        self.name = name
        self.filepath = filepath
        self.text = None

    def doc2text(self):
        doc = docx.Document(self.filepath)
        self.par_list = [paragraph.text for paragraph in doc.paragraphs if paragraph.text != '']
        self.text = ' '.join(self.par_list)

    def pipe(self, func=None):
        self.doc2text()


class DownloadWorker(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            file = self.queue.get()
            try:
                file.pipe()
            finally:
                self.queue.task_done()

if __name__ == '__main__':
    path_to_docs = Path('')
    path_to_table = Path('')
    path_to_output_data = Path('')
    number_files = 0

    path_params_parts =  os.path.abspath(__file__).split('\\')
    path_params_parts.remove(path_params_parts[len(path_params_parts)-1])
    path_dir = '\\'.join(path_params_parts)
    path_params = path_dir + "\\params.yaml"

    with open(path_params, 'r', encoding='utf8') as fd:
        params = yaml.safe_load(fd)

    path_to_docs = Path(str(params['pdocs']))
    path_to_table = Path(str(params['ptable']))
    if params['p'] is not None:
        path_to_output_data = Path(str(params['p']))
    number_files = int(params['n'])

    data = pd.read_excel(path_to_table)

    # формирурем список файлов, у которых больше 200 упоминаний
    lables = list(topic[0] for topic in data.delo.value_counts().to_dict().items() if topic[1] > 200)
    data = data.loc[data['delo'].isin(lables)]

    path_files = list(path_to_docs.iterdir())

    data = data.assign(name=data['Наименование файла'].map(lambda x: Path(x).stem))

    # смотрим какие файлы есть и в таблице и в директории
    table_namespace = set(data.name.tolist())
    file_namespace = set(path_file.stem for path_file in path_files)
    diff1 = table_namespace & file_namespace

    path_files_valid = {path_file.stem: path_file for path_file in path_files if path_file.stem in diff1}

    # создаем словарь из имени файлы и самого обьекта
    files = {}
    for fil in path_files_valid.items():
        files[fil[0]] = FileData(fil[0], fil[1])

    if number_files == 0:
        number_files = len(files)

    files_cut = dict(list(files.items())[:number_files])
    # считываем файлы в обьекты
    print('loading files')
    # Create a queue to communicate with the worker threads
    queue = Queue()
    # Create 8 worker threads
    for x in range(8):
        worker = DownloadWorker(queue)
        # Setting daemon to True will let the main thread exit even though the workers are blocking
        worker.daemon = True
        worker.start()

    for file in files_cut.values():
        queue.put(file)

    with tqdmn(total=len(files_cut)) as pbar:
        last_readed_files = queue.unfinished_tasks
        while queue.unfinished_tasks != 0:
            pbar.update(last_readed_files - queue.unfinished_tasks)
            last_readed_files = queue.unfinished_tasks
            time.sleep(0.2)

    queue.join()

    data_delo_names = data.loc[:, ['delo', 'name']]
    dirty_text = pd.DataFrame([[key, value.text, ] for key, value in tqdmn(files_cut.items())],
                              columns=['name', 'text'])
    data_output = pd.merge(dirty_text, data_delo_names, on='name')

    # files_output = {name: file.text for name, file in files.items() if file.text is not None}

    with open(path_dir + '\\files.pickle', 'wb') as f:
        pickle.dump(data_output, f)

