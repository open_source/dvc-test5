from pathlib import Path
import sys
import pandas as pd
from tqdm import tqdm as tqdmn
import time
import pickle
import requests
from sklearn.decomposition import PCA, FastICA, TruncatedSVD

import yaml
import os

def chunker(seq, size):
    for pos in range(0, len(seq), size):
        yield seq.iloc[pos:pos + size]


def get_composed_data(composed_method, n_components, df):
    alg = composed_method(n_components=n_components)
    alg.fit(df)
    compress_train = alg.transform(df)
    return pd.DataFrame(compress_train, index=df.index)


def get_bert_vectors(addr, port, text_collection: pd.Series):
    session = requests.Session()
    session.trust_env = False
    resp = session.post(f'{addr}:{port}/text2vec', json={'texts': text_collection.tolist()})
    return pd.DataFrame(resp.json()['vecs'], index=text_collection.index)




if __name__=='__main__':

    uri_sbert_service = "http://161.8.107.135"
    port_sbert_service = 5055
    method_reduction = 'pca'
    path_to_output_data = Path('')
    path_data = Path('data.pickle')
    number_components = 10

    path_params_parts =  os.path.abspath(__file__).split('\\')
    path_params_parts.remove(path_params_parts[len(path_params_parts)-1])
    path_dir = '\\'.join(path_params_parts)

    path_params = path_dir + "\\params.yaml"

    with open(path_params, 'r', encoding='utf8') as fd:
        params = yaml.safe_load(fd)

    uri_sbert_service = str(params['uri'])
    port_sbert_service = str(params['port'])
    method_reduction = str(params['method_reduction'])
    number_components = int(params['number_components'])
    path_data = Path(str(params['path_data']))

    if params['path_to_output_data'] is not None:
        path_to_output_data = Path(str(params['path_to_output_data']))

    with open(path_data, 'rb') as f:
        data = pickle.load(f)

    # получаем вектор на каждый текст

    print('calc text2vec vectors')
    bert_vectors = get_bert_vectors(uri_sbert_service, port_sbert_service,  data.text[:100])
    for i in tqdmn(chunker(data[100:], 100)):
        bert_vectors = pd.concat([bert_vectors, get_bert_vectors(uri_sbert_service, port_sbert_service, i.text)], axis=0)

    data_names = data.loc[:, 'name']

    new_table = pd.concat([data_names, bert_vectors], axis=1)
    new_table = new_table.dropna()

    # производим уменьшение вектора текста и сохраняем модель
    if method_reduction == 'pca':
        alg = PCA(n_components=number_components)
        alg.fit(new_table.iloc[:, 1:])
        with open(path_dir+'\\t2v_model_reduction_pca', 'wb') as f:
            pickle.dump(alg, f)


    elif method_reduction == 'svd':
        alg = TruncatedSVD(n_components=number_components)
        alg.fit(new_table.iloc[:, 1:])
        with open(path_dir +'\\t2v_model_reduction_svd', 'wb') as f:
            pickle.dump(alg, f)


    elif method_reduction == 'fastica':
        alg = FastICA(n_components=number_components)
        alg.fit(new_table.iloc[:, 1:])
        with open(path_dir +'\\t2v_model_reduction_fastica', 'wb') as f:
            pickle.dump(alg, f)

    #TODO назвать колонки по именам фичей
    table_compress = alg.transform(new_table.iloc[:, 1:])
    bert_tabel = pd.concat([new_table.iloc[:, :1], pd.DataFrame(table_compress, index=new_table.index)], axis=1)

    bert_tabel.columns = [str(fea) + '_t2v' if fea not in ['name', 'text', 'delo'] else fea for fea in
                          bert_tabel.columns]

    print('data processed')
    with open(path_dir + '\\bert_table.pickle', 'wb') as f:
        pickle.dump(bert_tabel, f)